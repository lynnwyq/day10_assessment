//Create an IIFE
(function () {

    //Create an angular app/module 
    //[This has to linked within <html> in html using ng-app="<appName>"]
    var RegisterApp = angular.module("RegisterApp", []);


    //Create a service
    var countrySvc = function ($http, $rootScope, $q) {
        var countrySvc = this;
        countrySvc.country_list = [];
    }

    countrySvc.loadCountry_list = function () {
        $http.get("/country_list")
            .then(function (result) {
                countrySvc.country_list = result.data;
                $rootScope.$broadcast("countrySvc.country_listLoaded");
            });
    }

    countrySvc.loadCountry_list();


	//$q is the defer/promise service
	countrySvc.$inject = ["$http", "$rootScope", "$q"]






//Create a function constructor to be used as our controller 
//[This has to be linked within all <input>, <select>, <textarea> in html using ng-model="<instanceName>.<fieldName>"]
var RegisterCtrl = function (countrySvc, $scope) {
    var registerCtrl = this;

    $scope.$on("countrySvc.country_listLoaded", function () {
        registerCtrl.country_list = countrySvc.country_list;
    });

    registerCtrl.country_list = [];

    registerCtrl.name = "";
    registerCtrl.gender = "";
    registerCtrl.dob = "";
    registerCtrl.address = "";
    registerCtrl.country = "";
    registerCtrl.phone = "";
    registerCtrl.email = "";
    registerCtrl.password = "";

    /*This is if I want to have the clear function
            registerCtrl.clear = function () {
                registerCtrl.name = "";
                registerCtrl.gender = "";
                registerCtrl.dob = "";
                registerCtrl.address = "";
                registerCtrl.country = "";
                registerCtrl.phone = "";
                registerCtrl.email = "";
                registerCtrl.password = "";
            }
    */

    //Function to validate age
    registerCtrl.validateDOB = function validate(dob) {
        var today = new Date();
        var yyyy = today.getFullYear()

        if (!birthday.isValid()) {
            registerCtrl.DOBmessage = "Invalid date";
        }
        else if (eighteenYearsAgo.isAfter(birthday)) {
            registerCtrl.DOBmessage = "Okay, you're good";
        }
        else {
            registerCtrl.DOBmessage = "Sorry, you are underage";
        }
    }

    jsprint(validate, "4/10/1995");

    //[This has to linked within <form> in html using novalidate ng-submit="<instanceName>.submit()"]
    //[Note that $http.get("/GiveAName", {}) is to create a "fake database" to check for duplicate registration based on parameters provided]
    registerCtrl.submit = function () {
        $http.get("/registry", {
            params: {
                name: registerCtrl.name,
                email: registerCtrl.email,
            }
        }).then(function (result) {
            registerCtrl.message = "Your registration id is " + result.data.regId;
        }).catch(function () {
            registerCtrl.message = registerCtrl.email + " has been registered";
        })
    }
};

//Inject is needed to activate the service resource
RegisterCtrl.$inject = ["countrySvc", "$scope"]

RegisterApp.service("countrySvc", countrySvc);


//Register function constructor as controller
//[This has to be linked within <body> in html using ng-controller="<controllerName> as <instanceName>"]
RegisterApp.controller("RegisterCtrl", RegisterCtrl);

})();